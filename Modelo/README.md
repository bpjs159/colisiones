# Modelo #

Este es el modelo y la implementaci�n, para funcionamiento de una colision.

### Explicaci�n de Funcionamiento ###

El modelo se contruyo con base al modelo MVC. Partiendo desde all�, el Controlador dependia directamente de Pelota, quien tiene como atributos posicionX, posicionY, radio, aceleracion, direccionX y direccionY, teniendo en cuenta estas caracteristicas el Controlador va a chequear que las pelotas no se salgan del espacio destinado para el manejo de las mismas. Con est� informaci�n, dicho Controlador se comunica con el Modelo para que �ste se actualize y verifique las colisiones de las Pelotas, el cual validando la distancia entre cada una de ellas haciendo uso del teorema de pit�goras, va a notificar si estan colisionando o no, esta notificaci�n se reflejara en la vista en medida de que si las pelotas estan colisionando estar�n en color rojo y si �stas estan libres estaran de color verde.

### Modelo ###

* Se encuentra en Modelo/Diagrama de Clases.jpeg

### Integrantes ###

* 20141020053 Johan Sebastian Bonilla Plata
* 20141020096 Javier Duvan Hospital Melo
* 20141020135 Miguel �ngel Hern�ndez Cubillos