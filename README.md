# Modelo #

Este es el modelo y la implementación, para funcionamiento de una colision.

### Explicación de Funcionamiento ###

El modelo se contruyo con base al modelo MVC. Partiendo desde allí, el Controlador dependia directamente de Pelota, quien tiene como atributos posicionX, posicionY, radio, aceleracion, direccionX y direccionY, teniendo en cuenta estas caracteristicas el Controlador va a chequear que las pelotas no se salgan del espacio destinado para el manejo de las mismas. Con está información, dicho Controlador se comunica con el Modelo para que éste se actualize y verifique las colisiones de las Pelotas, el cual validando la distancia entre cada una de ellas haciendo uso del teorema de pitágoras, va a notificar si estan colisionando o no, esta notificación se reflejara en la vista en medida de que si las pelotas estan colisionando estarán en color rojo y si éstas estan libres estaran de color verde.

### Modelo ###

* Se encuentra en Modelo/Diagrama de Clases.jpeg

### Integrantes ###

* 20141020053 Johan Sebastian Bonilla Plata
* 20141020096 Javier Duvan Hospital Melo
* 20141020135 Miguel Ángel Hernández Cubillos