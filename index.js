
var lienzo = document.getElementById("canva");
var colorCirculos = "#66BB6A";

class Controlador {
  constructor(modelo,pelotaA,pelotaB) {
  	this.modelo = modelo;
    this.pelotaA = pelotaA;
    this.pelotaB = pelotaB;
  }

  realidad() {

  	
  	   this.realidad = setInterval(function(){
  	   	
  	   	if((pelotaA.posicionX<=pelotaA.radio)){
            pelotaA.direccionX=1;
            pelotaA.posicionX += pelotaA.aceleracion;
   			

  		}else if((lienzo.width-pelotaA.posicionX)<=pelotaA.radio){
              pelotaA.direccionX=-1;
              pelotaA.posicionX -= pelotaA.aceleracion;
  			

  		}else if ((pelotaA.posicionY<=pelotaA.radio)){
            pelotaA.direccionY=1;
            pelotaA.posicionY += pelotaA.aceleracion;
  			

  		}else if((lienzo.height-pelotaA.posicionY)<=pelotaA.radio){
            pelotaA.direccionY=-1;
            pelotaA.posicionY -= pelotaA.aceleracion;
  			

  		}else{
  			if(pelotaA.direccionX==1){
  				pelotaA.posicionX += pelotaA.aceleracion;
  			}else if(pelotaA.direccionX==-1){
  				pelotaA.posicionX -= pelotaA.aceleracion;
  			}
  			if(pelotaA.direccionY==1){
  				pelotaA.posicionY += pelotaA.aceleracion;
  			}else if(pelotaA.direccionY==-1){
  				pelotaA.posicionY -= pelotaA.aceleracion;
  			}
  			
  			
  		
  		}


  			if((pelotaB.posicionX<=pelotaB.radio)){
            pelotaB.direccionX=1;
            pelotaB.posicionX += pelotaB.aceleracion;
   			

  		}else if((lienzo.width-pelotaB.posicionX)<=pelotaB.radio){
              pelotaB.direccionX=-1;
              pelotaB.posicionX -= pelotaB.aceleracion;
  			

  		}else if ((pelotaB.posicionY<=pelotaB.radio)){
            pelotaB.direccionY=1;
            pelotaB.posicionY += pelotaB.aceleracion;
  			

  		}else if((lienzo.height-pelotaB.posicionY)<=pelotaB.radio){
            pelotaB.direccionY=-1;
            pelotaB.posicionY -= pelotaB.aceleracion;
  			

  		}else{
  			if(pelotaB.direccionX==1){
  				pelotaB.posicionX += pelotaB.aceleracion;
  			}else if(pelotaB.direccionX==-1){
  				pelotaB.posicionX -= pelotaB.aceleracion;
  			}
  			if(pelotaB.direccionY==1){
  				pelotaB.posicionY += pelotaB.aceleracion;
  			}else if(pelotaB.direccionY==-1){
  				pelotaB.posicionY -= pelotaB.aceleracion;
  			}
  			
  			
  		
  		}


  		

  		this.modelo.actualizarModelo(this.pelotaA,this.pelotaB);
  		this.modelo.controlarColision(this.pelotaA,this.pelotaB);
  	},10);

  	
  }
  
}


class Pelota {
  constructor(posicionX,posicionY,radio,aceleracion,direccionX,direccionY) {
    this.posicionX = posicionX;
    this.posicionY = posicionY;
    this.radio = radio;
    this.aceleracion = aceleracion;
    this.direccionX=direccionX;
    this.direccionY=direccionY;
  }

}


class Modelo {
  constructor(vista) {
  	this.vista = vista;
  	var canvas = document.getElementById("canva");
  	var ctx = canvas.getContext("2d");
	  this.vista.setCtx(ctx);
	  

  	}
  	actualizarModelo(pelotaA,pelotaB){

		  this.vista.limpiar();
		  
		this.vista.hacerCuadrado();
  		this.vista.dibujar(pelotaA);
  		this.vista.dibujar(pelotaB);
  		
  		
  	}

  	controlarColision(pelotaA,pelotaB){
  		

  		var a=pelotaA.posicionX-pelotaB.posicionX;
  		var b=pelotaA.posicionY-pelotaB.posicionY;
		var r=pelotaA.radio+pelotaB.radio;
		var h = Math.sqrt((Math.pow(a,2))+(Math.pow(b,2)));
  		
  		if(h<=r){
			colorCirculos = "#FF5722";
		  }else{
			colorCirculos = "#66BB6A";
		  }
  	}
  

}





class Vista {
  constructor() {
  	this.ctx = null;
  	}

  	setCtx(ctx){
		  this.ctx=ctx;
  	}

  	dibujar(pelota){


  		this.ctx.beginPath();
		this.ctx.arc(pelota.posicionX, pelota.posicionY, pelota.radio, 0, 2 * Math.PI);
		this.ctx.stroke();
		this.ctx.fillStyle = colorCirculos;
		this.ctx.fill();

		
	  }
	  
	  hacerCuadrado(){
		  
		this.ctx.fillStyle = "#E0E0E0";
		this.ctx.fillRect(0, 0, lienzo.width, lienzo.height);
	  }

  	limpiar(){
		this.ctx.clearRect(0, 0, 800, 800);
  	}
  	
  

}






vista = new Vista();
modelo = new Modelo(vista);

pelotaA = new Pelota(600,200,100,1,1,1);
pelotaB = new Pelota(200,200,100,1,1,1);
control = new Controlador(modelo,pelotaA,pelotaB);
control.realidad();

